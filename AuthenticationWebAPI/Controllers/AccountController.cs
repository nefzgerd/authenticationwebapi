﻿using BC = BCrypt.Net.BCrypt;
using AuthenticationWebAPI.Data;
using Microsoft.AspNetCore.Mvc;
using AuthenticationWebAPI.Models;

namespace AuthenticationWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly WebshopContext _context;

        public AccountController(WebshopContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register(RegisterViewModel model)
        {
            var isEmailRegistered = _context.Users.Any(x => x.Email == model.Email);

            if (!isEmailRegistered)
            {
                if (ModelState.IsValid)
                {
                    ModelState.Clear();

                    var user = new User()
                    {
                        Username = model.Username,
                        Email = model.Email,
                        Password = BC.HashPassword(model.Password),
                        Role = 1
                    };

                    _context.Users.Add(user);
                    _context.SaveChanges();

                    return Ok("Registration successful.");
                }
            }
            else
            {
                ModelState.AddModelError(nameof(RegisterViewModel.Email), "Email is already in use.");
            }

            return UnprocessableEntity(ModelState);
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginViewModel model)
        {
            var isEmailRegistered = _context.Users.Any(x => x.Email == model.Email);

            if (isEmailRegistered)
            {
                var correspondingPassword = _context.Users.Where(x => x.Email == model.Email).SingleOrDefault()!.Password;

                if (ModelState.IsValid && BC.Verify(model.Password, correspondingPassword))
                {
                    ModelState.Clear();

                    return Ok("Authentication successful.");
                }
                else
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Password), "The password you have entered is incorrect.");
                }
            }
            else
            {
                ModelState.AddModelError(nameof(LoginViewModel.Email), "The email is not connected to an account.");
            }

            return UnprocessableEntity(ModelState);
        }
    }
}
