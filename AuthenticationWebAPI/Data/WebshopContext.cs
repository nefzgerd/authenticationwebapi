﻿using AuthenticationWebAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthenticationWebAPI.Data
{
    public class WebshopContext : DbContext
    {
        public WebshopContext(DbContextOptions<WebshopContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; } = null!;
    }
}
